name := "wikibrain-scala"

version := "1.0"

scalaVersion := "2.11.6"

unmanagedJars in Compile += file("lib/wikibrain-withdeps-0.5.2.jar")