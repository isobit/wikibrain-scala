import org.wikibrain.wikidata.LocalWikidataStatement
import wikibrain.EasyWikiBrain

object Main {
  def main(args: Array[String]): Unit = {
    val easyWikiBrain = new EasyWikiBrain(args)
    (
      easyWikiBrain.findPageByTitle("Minnesota"),
      easyWikiBrain.findPageByTitle("Pennsylvania"),
      easyWikiBrain.findPageByTitle("California"),
      easyWikiBrain.findPageByTitle("University of Minnesota"),
      easyWikiBrain.findPageByTitle("Minneapolis")
      ) match {
      case (Some(mn), Some(pa), Some(ca), Some(umn), Some(mpls)) =>
        println(easyWikiBrain.similarity(mn, umn))
        println(easyWikiBrain.similarity(mpls, umn))
        println(easyWikiBrain.similarity(pa, umn))
        println(easyWikiBrain.similarity(ca, umn))
        println(s"${mn.title}: ${mn.inlinks().length}")
        println(s"${mpls.title}: ${mpls.inlinks().length}")
        println(s"${pa.title}: ${pa.inlinks().length}")
        println(s"${ca.title}: ${ca.inlinks().length}")
        println(s"${umn.title}: ${umn.inlinks().length}")
        mpls.statements.map({
          case (s, l) =>
            println(s)
            println(l)
        })
      case _ => println("Something went wrong!")
    }
  }
}
