package wikibrain

import java.util

import org.wikibrain.wikidata.{WikidataDao, LocalWikidataStatement}

import scala.collection.JavaConversions._

import com.vividsolutions.jts.geom.Geometry
import org.geotools.referencing.GeodeticCalculator
import org.wikibrain.core.cmd.EnvBuilder
import org.wikibrain.core.dao.{UniversalPageDao, LocalLinkDao, LocalPageDao}
import org.wikibrain.core.lang.Language
import org.wikibrain.core.model.{LocalLink, LocalPage}
import org.wikibrain.phrases.PhraseAnalyzer
import org.wikibrain.spatial.dao.{SpatialContainmentDao, SpatialDataDao}
import org.wikibrain.sr.SRMetric

class EasyWikiBrain(args: Array[String]) {

  lazy val env = EnvBuilder.envFromArgs(args)
  lazy val conf = env.getConfigurator
  lazy val lpDao: LocalPageDao = conf.get(classOf[LocalPageDao])
  lazy val llDao: LocalLinkDao = conf.get(classOf[LocalLinkDao])
  lazy val sdDao: SpatialDataDao = conf.get(classOf[SpatialDataDao])
  lazy val scDao: SpatialContainmentDao = conf.get(classOf[SpatialContainmentDao])
  lazy val upDao: UniversalPageDao = conf.get(classOf[UniversalPageDao])
  lazy val wdDao: WikidataDao = conf.get(classOf[WikidataDao])
  lazy val pa: PhraseAnalyzer = conf.get(classOf[PhraseAnalyzer], "anchortext")
  lazy val metric: SRMetric = conf.get(classOf[SRMetric], "ensemble", "language", "simple")
  lazy val geoCalc = new GeodeticCalculator()

  class Page(val localPage: LocalPage) {
    lazy val title: String = localPage.getTitle.getCanonicalTitle
    lazy val localId: Int = localPage.getLocalId
    lazy val univId: Int = upDao.getByLocalPage(localPage).getUnivId
    lazy val statements: Map[String, List[LocalWikidataStatement]] =
      wdDao.getLocalStatements(localPage).toMap.map({
        case (s, l) => (s, l.toList)
      })
    def geometry(s: String = "wikidata"): Geometry = sdDao.getGeometry(upDao.getByLocalPage(localPage).getUnivId, s)
    def inlinks(lang: Language = localPage.getLanguage): Iterator[LocalLink] =
      llDao.getLinks(lang, localId, false, true, LocalLink.LocationType.NONE).iterator()
  }

  def findPageByTitle(title: String, language: Language = Language.SIMPLE): Option[Page] =
    lpDao.getByTitle(language, title) match {
      case null => None
      case localPage: LocalPage => Some(new Page(localPage))
    }

  /*
  Get a distance in kilometers between two geometries
   */
  def distance(a: Geometry, b: Geometry): Double = {
    geoCalc.setStartingGeographicPoint(a.getCentroid.getX, a.getCentroid.getY)
    geoCalc.setDestinationGeographicPoint(b.getCentroid.getX, b.getCentroid.getY)
    geoCalc.getOrthodromicDistance / 1000
  }

  def similarity(a: Page, b: Page): Double =
    metric.similarity(a.localId, b.localId, false).getScore

}
